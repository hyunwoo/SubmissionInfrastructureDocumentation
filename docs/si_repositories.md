# Submission Infrastructure repositories
This is a list of repositories that are relevant for the SI operations:

 * **Puppet repositories**. 
    * [gitlab.cern.ch/ai/it-puppet-hostgroup-vocms](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocms). One of the most important
    of the puppet repositories. Here the configuration of puppetized machines is managed.
    * [gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsglidein). This is the
    old version of the above repository (vocms), it is currently used only by CRAB schedds.
    * [gitlab.cern.ch/ai/it-puppet-module-vocmshtcondor](https://gitlab.cern.ch/ai/it-puppet-module-vocmshtcondor). All the condor
    configuration comes form this puppet module.
    * [gitlab.cern.ch/ai/it-puppet-module-vocmsgwmsfrontend](https://gitlab.cern.ch/ai/it-puppet-module-vocmsgwmsfrontend). The 
    configuration for GlideinWMS Frontend comes from this module.
    * [gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsfactory](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsfactory). Factory 
    machines at CERN use this puppet hostgroup


 * **Submission Infrastructure**. These repositories are grouped under the [CMSSI](https://gitlab.cern.ch/CMSSI) gitlab group
    * [gitlab.cern.ch/CMSSI/SubmissionInfrastructureScripts](https://gitlab.cern.ch/CMSSI/SubmissionInfrastructureScripts). This is 
    a git lab repository to store custom scripts being utilized in CMS submission infrastructure operations.
    * [gitlab.cern.ch/CMSSI/SubmissionInfrastructureMonitoring](https://gitlab.cern.ch/CMSSI/SubmissionInfrastructureMonitoring). Here the
    script regarding SI monitoring are kept. See the section [SI Monitoring](../si_monitoring/README.md)
    * [gitlab.cern.ch/CMSSI/cmsgwms-htcondor-configurations](https://gitlab.cern.ch/CMSSI/cmsgwms-htcondor-configurations). In this
    repository we are suppose to have all the condor configuration, which is now under the htcondor puppet module "vocmshtcondor"
    listed above. This is a projecto for the future in which Fermilab backup nodes and CERN ones will share the same condor configs
    * [gitlab.cern.ch/CMSSI/cmsgwms-frontend-configurations](https://gitlab.cern.ch/CMSSI/cmsgwms-frontend-configurations). This 
    repository holds the main configuration file for the GlideinWMS Frontends. There are two branches "cern" and "fnal", as you
    can imagine CERN Frontends point ot the former branch and  FNAL backup Frontends to the latest one.
    All the different Frontends from the different pools (global, itb, cern, etc) share the same branch, for that reason we have
    a non-written rule that every commit message should have a prefix depending on the pool in wich the change was applied
    e.g. "ITB: New schedd added to the itb frontend"
    * [gitlab.cern.ch/CMSSI/CMSglideinWMSValidation](https://gitlab.cern.ch/CMSSI/CMSglideinWMSValidation). In this repository
    are stored most of the "validation scripts" used by the pilots to "validate" the environment of a worker node. Notice that there
    are other validation scripts that are set by the Factory and those are not here.

