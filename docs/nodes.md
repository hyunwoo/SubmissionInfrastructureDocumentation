<link rel="stylesheet" href="link/to/stylesheet" />
<style>
  @media only screen and (min-width: 76.25em) {
  .md-main__inner {
    max-width: none;
  }
  .md-sidebar--primary {
    left: 0;
  }
  .md-sidebar--secondary {
    right: 0;
    margin-left: 0;
    -webkit-transform: none;
    transform: none;   
  }
}
</style>

# Submission Infrastrucure Nodes (Automaticaly updated)

## CERN MachinesPool|Hostname|Hostgroup|Environment|Condor Version|GWMS Version
----|--------|---------|-----------|--------------|------------
itb|vocms0803.cern.ch|vocmssi/si_htcondor/ccb/itb|cmssi_scaletest|10.0.1|
itb|vocms0824.cern.ch|vocmssi/schedd_htcondor/scale_test/itb|cmssi_scaletest|10.0.1|
itb|vocms0826.cern.ch|vocmssi/schedd_htcondor/scale_test/itb|cmssi_scaletest|10.0.1|
condormon|vocms0852.cern.ch|vocmssi/si_htcondor/condormon|qa|10.0.1|
itb|vocms0837.cern.ch|vocmssi/schedd_htcondor/scale_test/itb|cmssi_scaletest|10.0.1|
itbdev|vocms0823.cern.ch|vocmssi/schedd_htcondor/si_test/itbdev|cmssi_scaletest|10.0.1|
itb|vocms0829.cern.ch|vocmssi/schedd_htcondor/scale_test/itb|cmssi_scaletest|10.0.1|
itb|vocms0835.cern.ch|vocmssi/schedd_htcondor/scale_test/itb|cmssi_scaletest|10.0.1|
itb|vocms0828.cern.ch|vocmssi/schedd_htcondor/scale_test/itb|cmssi_scaletest|10.0.1|
itb|vocms0827.cern.ch|vocmssi/schedd_htcondor/scale_test/itb|cmssi_scaletest|10.0.1|
itb|vocms0832.cern.ch|vocmssi/schedd_htcondor/scale_test/itb|cmssi_scaletest|10.0.1|
itb|vocms0833.cern.ch|vocmssi/schedd_htcondor/scale_test/itb|cmssi_scaletest|10.0.1|
itb|vocms0836.cern.ch|vocmssi/schedd_htcondor/scale_test/itb|cmssi_scaletest|10.0.1|
itb|vocms0808.cern.ch|vocmssi/si_htcondor/central_manager/itb|cmssi_scaletest|10.0.1|
itb|vocms0834.cern.ch|vocmssi/schedd_htcondor/scale_test/itb|cmssi_scaletest|10.0.1|
volunteer|vocms0840.cern.ch|vocmssi/si_htcondor/central_manager/volunteer|qa|9.2.0|3.6.5
itb|vocms4100.cern.ch|vocmssi/schedd_htcondor/scale_test/itb|cmssi_scaletest|10.0.1|
global|vocms0813.cern.ch|vocmssi/si_htcondor/ccb/global|production|10.0.1|
itb|vocms0809.cern.ch|vocmssi/schedd_htcondor/scale_test/itb|cmssi_scaletest|9.2.0|
itbdev|vocms0801.cern.ch|vocmssi/glideinwms/gwmsfrontend/itbdev|qa|10.0.1|
itb|vocms0825.cern.ch|vocmssi/schedd_htcondor/scale_test/itb|cmssi_scaletest|10.0.1|
tier0|vocms005.cern.ch|vocmssi/schedd_htcondor/si_test/tier0|qa|9.1.3|
itb|vocms0830.cern.ch|vocmssi/schedd_htcondor/scale_test/itb|cmssi_scaletest|10.0.1|
itb|vocms0831.cern.ch|vocmssi/schedd_htcondor/scale_test/itb|cmssi_scaletest|10.0.1|
itb|vocms0802.cern.ch|vocmssi/glideinwms/gwmsfrontend/itb|qa|10.0.1|3.10.0
itb|vocms0811.cern.ch|vocmssi/schedd_htcondor/si_test/itb|qa|10.0.1|
monitor_antonio|vocms0851.cern.ch|vocmssi/si_htcondor/monitor_antonio|qa|10.0.1|
global|vocms0814.cern.ch|vocmssi/si_htcondor/central_manager/global|production|10.0.1|
tier0|vocms0819.cern.ch|vocmssi/glideinwms/gwmsfrontend/tier0|production|9.1.2|3.9.6
global|vocms080.cern.ch|vocmssi/glideinwms/gwmsfrontend/global|production|9.1.2|3.9.6
itb|vocms0822.cern.ch|vocmssi/schedd_htcondor/si_test/itb|cmssi_scaletest|10.0.1|
tier0|vocms0820.cern.ch|vocmssi/si_htcondor/central_manager/tier0|production|10.0.1|
tier0|vocms0821.cern.ch|vocmssi/si_htcondor/ccb/tier0|production|10.0.1|
itbdev|vocms0804.cern.ch|vocmssi/si_htcondor/central_manager/itbdev|qa|10.0.1|
## FNAL Machines

Pool|Hostname|Role|Condor Version|GWMS Version
----|--------|---------|-----------|--------------|------------
global|cmssrvz02.fnal.gov|Backup Collector & Negotiator|8.9.11-1||
global|cmssrvz03.fnal.gov|Condor connection broker|8.9.11-1||
global||frontend?|8.9.11-1||
itb|cmssrv623.fnal.gov|Backup Collector & Negotiator|8.9.11-1||
itb|??|Condor connection broker?|8.9.11-1||
itb|cmssrv216.fnal.gov|Backup frontend|8.9.11-1||
cern|cmssrv622.fnal.gov|Backup Collector & Negotiator|8.9.11-1||
cern|cmssrv601.fnal.gov|Condor connection broker|8.9.11-1||
cern|cmssrv246.fnal.gov|Backup frontend|8.9.11-1||

### FNAL Schedds

Pool | Host | Service | HTCondor version |
-----|------|---------|------------------|
itb| cmsgwms-submit1.fnal.gov | prod schedd, SL7 | 8.9.6-1 |
itb| cmsgwms-submit2.fnal.gov | prod schedd, SL7 | 8.9.6-1 |
global| cmsgwms-submit3.fnal.gov | prod schedd, SL7 | 8.9.11-1 |
global| cmsgwms-submit4.fnal.gov | prod schedd, SL7 | 8.9.11-1 |
global| cmsgwms-submit5.fnal.gov | prod schedd, SL7 | 8.9.11-1 |
global| cmsgwms-submit6.fnal.gov | prod schedd, SL7 | 8.9.11-1 |
global| cmsgwms-submit7.fnal.gov | prod schedd, SL7 | 8.9.11-1 |
global| cmsgwms-submit8.fnal.gov | prod schedd, SL7 | 8.9.11-1 |
