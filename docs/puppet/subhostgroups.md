# Subhostgroups

Here you will find a brief description of each of the pupet profiles used by
SI machines. 
Notice that empty files within the different profiles will not been mentioned.

The repository for each of the subhostgroups and the general
hostgroup "vocms" is this:
[it-puppet-hostgroup-vocms](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocms)




## Frontend - vocms/glideinwms/gwmsfrontend

The Frontend profile uses 2 subhostgroups: glideinwms and gwmsfrontend. At the
moment of writting this documentation, there is only one type of profile under
"glideinwms" so it makes no sense to have a subcategory only for the Frontend
profile but I think the idea is to have the Factory profile also under the
"glideinwms" category. This way "glideinwms" would take care of common 
configuration between the Frontend and the Factory and then the subcategories
would take care of specific service configurations.

The "glideinwms" subhostgroup is composed of the following files: 
1. glideinwms.pp. 
    * Calls the files inside the glideinwms directory
    * Calls the ganglia module.
2. (glideinwms/firewall.pp). Open the port 80 in the internal firewall
3. (glideinwms/rpms.pp). Install httpd and git
4. (glideinwms/services.pp). Makes sure httpd is running

The "glideinwms/gwmsfrontend" subhostgrop is composed of the following files:
1. glideinwms/gwmsfrontend.pp.
    * Calls the files inside the glideinwms/gwmsfrontend directory
    * Includes the "vocmsgwmsfrontend" module
2. glideinwms/gwmsfrontend/directories.pp. configure logrotate
    (I think this does not work)
3. glideinwms/gwmsfrontend/monitor.pp. Sets the lemon metric: 30134 to
    monitor disk usage under /data
4. glideinwms/gwmsfrontend/tuning.pp:
    * Sets various kernel parameters (sysctl)
    * Sets security limits(/et/security/limits.d)
    * Defines whether ipv6 should be enable or not (only enabled in qa)
    * Restricts the use of weak MACs and Ciphers on ssh
5. glideinwms/gwmsfrontend/volumes.pp. Make sure that the external volume
   is mounted under /data.


## Central manager - vocms/si_htcondor/central_manager

In this profile we can see that "central_manager" is a subcategory of
"si_htcondor" that in turn is a subcategory of "vocms".

The "si_htconodor" subhostgroup is composed of the following files:
1. si_htcondor.pp. 
    * Calls the files inside si_htcondor directory
    * Includes the "vocmshtcondor" module
    * Calls ganglia module
2. si_htcondor/tuning.pp.
    * Enables/Disables ipv6 
    * Makes sure the "network" service is running
    * Sets the security limits(/etc/security/limits.d) for condor using a
        template
3. si_htcondor/volumes.pp. Make sure that the external volume is mounted
    under /data.
4. si_htcondor/directories.pp. Makes sure that "/data" exists asnd has the
    right permissions
5. si_htcondor/repos.pp. Installs the repository and package for the DODAS
    Certificate Authority(CA)
6. si_htcondor/rpms. Installs git

The "si_htcondor/central_manager" subhostgroup is composed of the following
    files:

    
 * CCB: vocms/si_htcondor/ccb
 * Production schedd: vocms/production/schedd
 * Tier0 schedd: vocms/tier0/schedd
 * Analysis schedd: vocmsglidein/crab



