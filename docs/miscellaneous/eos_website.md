# EOS-website

It is on of the 
web services](https://cernbox-manual.web.cern.ch/cernbox-manual/en/web/project_website_content.html)
provided by CERN-IT

The eos-website it is a web server that points to a specific EOS folder which
is owned by us, more specifically by the
[service account](https://account.cern.ch/account/Help/?kbid=011010) "siserv".
Only this account and its owner have permission to write to this EOS folder
(/eos/project/c/cms-htcondor-monitor/www/) and any file put in that folder can
be accessed from this domain 
[cms-htcondor-monitor](http://cms-htcondor-monitor.web.cern.ch/cms-htcondor-monitor/)


In order for us to put files into the EOS folder a Kerberos token is needed 
for authentication. This token is generated when the "siserv" user logs-in 
within the cern domain but, how can we use this from within a cron job or if
we do not have the "siserv" password? for that we will use a "keytab".

A keytab is a file that stores a Kerberos token.
We use a keytab
(vocms0851:/data/srv/SubmissionInfrastructureMonitoring/letts/siserv.keytab)
to store the siserv credentials and create a kerberos token so that we can
copy files into the EOS folder

It is VERY important to keep "keytab" file in a safe place, for that reason it
is not kept in any repository and it would have to be created manually if the
service is moved to a different host. See [(here)](https://kb.iu.edu/d/aumh)
for more information about keytabs and how to create them.



