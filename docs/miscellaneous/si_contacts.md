# SI-Contacts

Following are the list of important contacts, which are required by SI team for coordination.

| Service /Node               	| Contact Persons                          	| Emails                                                           	|
|-----------------------------	|------------------------------------------	|------------------------------------------------------------------	|
| SI hosts at CERN            	| Saqib Haleem                             	| saqib.haleem@cern.ch                                             	|
| CERN & UCSD factories       	| Edita Kizinevič                          	| Edita.kizinevic@cern.ch                                          	|
| SI hosts at FNAL            	| Farrukh Aftab, Maria Acosta, Hyunwoo Kim 	| farrukh.aftab.khan@cern.ch,   macosta@fnal.gov, hyunwoo@fnal.gov 	|
| FNAL WMAgents schedds       	| Maria Acosta                             	| macosta@fnal.gov                                                 	|
| FNAL Collectors/CCBs        	| Hyunwoo Kim                              	| hyunwoo@fnal.gov                                                 	|
| FNAL factory operations     	| Merina Albert                            	| merina@fnal.gov                                                  	|
| HLT                         	| Diego Da Silva Gomes                     	| diego.da.silva.gomes@cern.ch                                     	|
|                             	| Duncan Rand                              	| duncan.rand@imperial.ac.uk                                       	|
| CMSConnect                  	| kenyi Paolo Hurtado Anampa               	| kenyi.paolo.hurtado.anampa@cern.ch                               	|
| MIT Schedd                  	| Zhangqier Wang                           	| wangzqe@mit.edu                                                  	|
|                             	| Benedikt Maier                           	| bmaier@mit.edu                                                   	|
|                             	| Chad Freer                               	| freerc@mit.edu                                                   	|
| DODAS                       	| Daniele Spiga                            	| Daniele.Spiga@cern.ch                                            	|
| Volunteer    Pool resources 	| Ivan Reid                                	| ivan.reid@brunel.ac.uk                                           	|
