# Submission Infrastructure Monitoring

At the moment, there are 3 different monitoring instances managed by Submission Infrastructure:
 1. James's Monitoring: [cms-htcondor-monitor.web.cern.ch/cms-htcondor-monitor/letts](https://cms-htcondor-monitor.web.cern.ch/cms-htcondor-monitor/letts)
 2. Antonio's Monitoring [submit-3.t2.ucsd.edu/CSstoragePath/aperez/HTML](http://submit-3.t2.ucsd.edu/CSstoragePath/aperez/HTML/)
 3. Monit
    * [Slots view](https://monit-grafana.cern.ch/d/StuCibYiz/cms-submission-infrastructure-slots-overview?orgId=11)
    * [Payload view](https://monit-grafana.cern.ch/d/R1q4NmEiz/cms-submission-infrastructure-payload-view?orgId=11)

All the scripts regarding these monitors, can be found at:

 [gitlab.cern.ch/CMSSI/SubmissionInfrastructureMonitoring](https://gitlab.cern.ch/CMSSI/SubmissionInfrastructureMonitoring).

This repository is currently cloned into vocms0850:/data/srv/SubmissionInfrastructureMonitoring, the setup of this host is managed by puppet by the subhostgroup [vocms/si_htcondor/condormon](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocms/blob/qa/code/manifests/si_htcondor/condormon.pp).

## James's Monitoring

It is composed by a set of scripts that query the 3 main pools (global, cern and itb) and
create an HTML file per pool to be posted on a eos-website. The main script is 
[Monitor.sh](https://gitlab.cern.ch/CMSSI/SubmissionInfrastructureMonitoring/blob/master/letts/Monitor.sh),
this script is called periodically (every hour) by a cronjob set by puppet in
[here](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocms/blob/qa/code/manifests/si_htcondor/condormon/cron.pp)


For information on how the HTML files, produced by this monitor, are copied to
the EOS-website or general information on the EOS-webiste, please go to 
[EOS-website](../miscellaneous/eos_website.md)


## Antonio's Monitoring

Currently in transition from submit-3.t2.ucsd.edu to a similar schema as James's monitoring

## Monit

This monitor uses the monitoring infrastructure provided by CERN-IT to store and present the data, this allows us to have a very
simple setup. A puppet cronjob will call the script vocms0850:/data/srv/SubmissionInfrastructureMonitoring/monit/Monitor.sh, which
will collect condor information of the different pools and push it to an Elastic Search end point. This script has to be executed
as root due to the fact that we use the host certificates (of vocms0850) for authentication.

The collection of condor data is done using the [python bindings](https://htcondor-python.readthedocs.io/en/latest/), which is a
python interface to HTCondor that makes querying the condor infrastructure easier and more efficient.

To push the collected data we use a slightly modified version of the stompAMQ.py script provided by WMCore (our version can be seen 
[here](https://gitlab.cern.ch/CMSSI/SubmissionInfrastructureMonitoring/blob/master/monit/StompAMQ.py), WMCore version is [here](https://github.com/dmwm/WMCore/blob/master/src/python/WMCore/Services/StompAMQ/StompAMQ.py)). This script acts
as an interface to the sompt library which is used to talk to the Apache ActiveMQ module, an open source messaging server. This
messaging system is managed NOT by the MONIT team but by the Messaging team at CERN-IT. The messaging team will make our data available to the MONIT team behind scenes.

Currently we send ClassAd attributes from 5 different condor components in our pools: Autocluster, Collector, Negotiator, Schedd and 
Startd. The list of attributes, of each of these components, that is sent can be seen in the SubmissionInfrastructureMonitoring repository under the folder "classAds".

The main script for this monitor is called "my_monitor.py" and can be seen [here](https://gitlab.cern.ch/CMSSI/SubmissionInfrastructureMonitoring/blob/master/monit/my_monitor.py).




