# Standard migration

In this document you will find the general steps to follow in orderto  migrate
a machine.

 1. **Backup**. First of all you need to backup anything wchich is not managed 
    by puppet and that is not stored under /data. /data is an external volume 
    that will be added to the new machine without any changes, so the files
    there are safe.

 2. **Review the specs**. Go to aiadm and check the flavor of the machine with the
    command:

    ```openstack server show <HOSTNAME>``
    This way you know which flavor to use when rebuilding the machine.

 3. **Kill the machine**. Go to aiadm and execute the following command:

    ```ai-kill <HOSTNAME>```

    Notice that killing a machine from openstack will not be enough, since it
    would not be removed from the foreman database

4. **Wait for DNS record to be cleaned**. In order to be able to reuse the same
   hostname you will need it to become available again. It takes few minutes.
   You can use the following command to know when it is ready:

   ```while (host <HOSTNAME> > /dev/null 2>&1); do clear; echo Waiting for the DNS record of $hostname to be cleared.; sleep 10;done```


5. **Update the .yaml file**. Change the glideinWMS and/or condor versions if
   necessary. Make sure the new versions exist in the repository website

6. **Rebuild the machine**. For this you will need the following information:
   * puppet hostgroup/sub-hostgroup(s). You can get this by going to foreman (judy.cern.ch) and searching for a hostname with the same hostgroup of the machine you are trying to create.
   * image to be used. To see tha available images, execute the following command in aiadm: ```openstack image list```
   * the environment (qa or production). Use "qa" if the machine is in the ITB or ITBDdev pool, "production" if in the "global" or the "cern" pool.
   * flavor. To see the available flavors, execute the following command from aiadm: ```openstack flavor list``` 
   Once you have all the data above, from aiadm execute the following command:

    ```ai-bs --foreman-hostgroup vocms/[HOSTGROUP]/[SUB-HOSTGROUP] --nova-image "[IMAGE]" --foreman-environment [ENVIRONMENT] --landb-mainuser cms-service-glideinwms-admins --landb-responsible cms-service-glideinwms-admins --nova-flavor [FLAVOR] [HOSTNAME]```
   
    e.g. ```ai-bs --foreman-hostgroup vocms/schedd_htcondor/si_test --nova-image "SLC6 - x86_64 [2018-06-22]" --foreman-environment qa --landb-mainuser cms-service-glideinwms-admins --landb-responsible cms-service-glideinwms-admins --nova-flavor m2.3xlarge  vocms0834.cern.ch```

7. **Attach volume**. Once the machine state is "ACTIVE" proceed to attach the
   external volume which is mounted in /data:
   ```openstack server add volume --device /dev/vdb [HOSTNAME] [VOLUME_NAME]```
   In order to get the volume name, you can execute:
   ```openstack volume list```

   The volume should have a name with the pattern [HOSTNAME]_data and it should
   be marked as  "available"
   To check the status of the machine, execute:
   ```openstack server show [HOSTANME]```
   and llok for the filed "status"

8. **Let puppet run few times**

9. **Restore permissions**. if any system using the machine used to have files
   in /data, you will need to set restore the owner of those files so that the
   system can access them. For example, in the case of a condor machine, the 
   logs are stored in /data (with a link from /var/log/condor) and the owner
   of these files needs to be set to condor in order for condor to start.
   Just make sure that the "ProcLog" and "KernelTuning" are always owned by 
   root
   ``` chown condor:condor /data/srv/glidecondor/condor_local/log/MasterLog```
   
