# Newbie screwups to avoid 
This is a list of things to avoid doing that might look harmless on the first sight but they have provoked disaster in the past.

 1. **Stopping puppet for too long.** First of all I don't remember exactly how long is "too long", I think the rule says 2 weeks.
    If puppet is disabled "puppet agent --disable" for longer thant this, the outside firewall will close the ports of the machine
    causing all kinds of issues. The way to solve this is by enabling puppet on te machine and waiting for the firewall rules to
    get renewed.
 2. **Forget to backup 99_local_tweaks.config .** When migrating a machine always remember that the configuration on the 
    99_local_tweaks file is not kept anyware but within the machine, so if a machine is removed you will lose this file.
    Always backup this file before removing a machine.

