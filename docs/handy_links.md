# Handy Links
# Last updated Dec,2020
The goal of this section is to put together a set of links to documents and
tools which are useful and commonly used.

### Very common links
 * [List of hosts](https://twiki.cern.ch/twiki/bin/view/CMS/GlideinWMSFrontendOpsNodeList)
 * [GlideinWMS frontend service & pilot certificates](https://twiki.cern.ch/twiki/bin/view/CMSPublic/CompOpsGlideinWMSCerts)
 * [OpenStack dashboard](https://openstack.cern.ch/project/)
 * [Foreman(Judy)](https://judy.cern.ch/) -Only accessible from CERN network
 * Monitoring
    * [Submission Infrastructure Monitoring](si_monitoring/README.md)
        * [SI Grafana/MonIT monitoring dashboard](https://monit-grafana.cern.ch/d/3mIiV1mZk/si-dashboard?orgId=11)
        * [Ganglia](http://vocms0801.cern.ch/ganglia/?r=hour&cs=&ce=&m=load_one&s=by+name&c=VOCMS&tab=m&vn=&hide-hf=false)
        * [James's Monitor](http://cms-htcondor-monitor.t2.ucsd.edu/letts/production.html)
        * [Antonio's Monitor](https://cms-htcondor-monitor.web.cern.ch/aperezca/HTML/)
    * [GWMSMON](https://cms-gwmsmon.cern.ch/)
 * [CMS GlideinWMS Critical Service Documentation ??](https://twiki.cern.ch/twiki/bin/viewauth/CMS/CMSCriticalServiceGlideinwms)

### Extra Documentation and Manuals
 * [CERN IT Cloud documentation](http://clouddocs.web.cern.ch/clouddocs/)
 * [CERN IT Puppet documentation](http://configdocs.web.cern.ch/configdocs/overview/system.html)
 * [CERN IT Monitoring website](http://monit.web.cern.ch/monit/)
 * [CERN IT Monitoring documentation](http://monit-docs.web.cern.ch/monit-docs/)
 * [HTCondor manual](http://research.cs.wisc.edu/htcondor/manual/)
    * [HTCondor manual/Configuration Macros](http://research.cs.wisc.edu/htcondor/manual/latest/ConfigurationMacros.html)
    * [HTCondor manual/ClassAd attributes](http://research.cs.wisc.edu/htcondor/manual/latest/ClassAdAttributes.html)
 * [GlideinWMS documentation](http://glideinwms.fnal.gov/doc.prd/index.html)
    * [GlideinWMS variables](http://glideinwms.fnal.gov/doc.prd/factory/custom_vars.html#vars)
    * [GlideinWMS Frontend documentation](http://glideinwms.fnal.gov/doc.prd/frontend/index.html)
        * [GlideinWMS Frontend configuration](http://glideinwms.fnal.gov/doc.prd/frontend/configuration.html)
    * [GlideinWMS Factory documentation](http://glideinwms.fnal.gov/doc.prd/factory/index.html)
 * [GIT](https://twiki.cern.ch/twiki/bin/view/CMS/GlideinWMSFrontendOpsGIT)
