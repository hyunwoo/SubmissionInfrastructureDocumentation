# Documentation
The documentation is based on [gitbook](https://www.gitbook.com). Which in
simple terms is a way to write and structure documentation using a git
repository that later will be transformed (using a plugin) into html files.

## The setup
The documentation setup can be divided in 2 parts:

A normal **git repository** that stores all the information about the
documentation in .md files (same syntaxis used to write README files for git
repositories) and a **EOS-website** that we use to make the documentation
available on the web at: http://cms-htcondor-monitor.web.cern.ch/cms-htcondor-monitor/\_book/

Repository is here:
[SubmissionInfrastructureDocumentation](https://gitlab.cern.ch/CMSSI/SubmissionInfrastructureDocumentation)

A nice tutorial on how to work with gitbooks is
 [here](https://toolchain.gitbook.com/setup.html)



## How to modify the documentation
Modifying the information within the git repository is the same as with any
any other repository. You can clone the repository anywhere and submit your
changes from there. The simplest way would be to use the already cloned 
repository on vocms0850:/data/srv/SubmissionInfrastructureDocumentation.

The trick comes when you want a change to be reflected
into the website.

The easiest way to get the latest version published on the website is to:
 1. Go to vocms0851:/data/srv/SubmissionInfrastructureDocumentation
 2. Pull the latest changes (git pull)
 3. Execute the "publish.sh" script

in summary, the easiest way to make a change in the documentation would be:
 1. Go to vocms0850:/data/srv/SubmissionInfrastructureDocumentation
 2. Pull the latest changes (git pull)
 3. Make the desired changes
 4. Execute the "publish.sh" script
 5. if you are not happy with the changes: GOTO 3, otherwisw: commit and push your changes
    1. git add (if a new file or directory was added)
    2. git commit -am "Meaningful message"
    3. git push origin master

### Important considerations abouth the "publish.sh" script
 * Needs acces to the "siserv.ketab" file which is at vocms0850:/data/srv
 * Has to be executed as root
 * It creates the html files using the current version of the local repository
   regardless you have committed and/or pushed your changes or not.
   Always make sure you have the latest version

The "publish.sh" script is included within the repository and cloned
automatically by puppet and does the following:

1. Creates the html files from the current version of your repository. This is
   done with the gitbook plugin, by executing the following command:

   ```gitbook build```

   That will create a "\_book" directory with all the html files of the
   documentation. **VERY IMPORTANT** this must be executed on the root of the
   repository, otherwise you will create a book only of the section of the
   directory in which you are.

2. Copies the full \_book directory into the EOS-website. For information
   on the EOS-website or how to make files available on it, go to
   [EOS-website](../miscellaneous/eos_website.md)


## Notes about the structure of the documentation

Only files that are listed within the SUMMARY.md file within the root of the
repository will be transformed into html files. If you try to reference a file
that is not in SUMMARY.md, it will not work.


